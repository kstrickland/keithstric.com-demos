package com.keithstric.demo.components;

import javax.faces.el.ValueBinding;

import com.ibm.xsp.component.xp.XspOutputLink;

public class CustomLink extends XspOutputLink {

	public static final String COMPONENT_TYPE = "com.keithstric.CustomLink";
	public static final String RENDERER_TYPE = "com.keithstric.demo.CustomLink";

	private String type;

	public CustomLink() {
		setRendererType("RENDERER_TYPE");
	}

	@Override
	public String getType() {
		if (null != type) {
			return type;
		}
		ValueBinding vb = getValueBinding("type");
		if (vb != null) {
			return (String) vb.getValue(getFacesContext());
		} else {
			return null;
		}
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}
}
