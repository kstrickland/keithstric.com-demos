package com.keithstric.demo.data;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import javax.faces.context.FacesContext;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.Item;
import lotus.domino.NotesException;

import com.ibm.xsp.designer.context.ServletXSPContext;
import com.ibm.xsp.designer.context.XSPUrl;
import com.ibm.xsp.extlib.util.ExtLibUtil;
import com.keithstric.demo.interfaces.DataObjectEx;
import com.keithstric.demo.utils.JSFUtil;

/**
 * This bean was originally from : http://www.mindoo.com/web/blog.nsf/dx/18.03.2011104725 KLEDH8.htm?opendocument&comments#anc1 and has been
 * modified to handle data for this demo. This bean handles all retrieving and saving of data
 */

public class DataProvider implements DataObjectEx, Serializable {

	private static final long serialVersionUID = 1L;
	public Map<String, Object> cachedValues;
	public Map<String, Object> changedValues;
	private String docUNID;
	private boolean isNewDoc = false;

	/**
	 * DataProvider Constructor - we setup the cachedValues and changedValues
	 */
	public DataProvider() {
		cachedValues = new HashMap<String, Object>();
		changedValues = new HashMap<String, Object>();
	}

	/**
	 * Get the current instance of the DataProvider bean
	 * 
	 * @return DataProvider - the current instance
	 */
	public static DataProvider getCurrentInstance() {
		return (DataProvider) JSFUtil.getVariableValue("data");
	}

	/**
	 * Get the UNID of the document we're currently processing
	 * 
	 * @return String - lotus.domino.Document.UniversalID
	 */
	@SuppressWarnings("unchecked")
	public String getDocUNID() {
		if (docUNID == null) {
			Map paramValues = (Map) JSFUtil.getVariableValue("paramValues");
			String[] documentIdArr = (String[]) paramValues.get("documentId");
			if (documentIdArr == null || documentIdArr.length == 0) {
				String[] documentUNIDArr = (String[]) paramValues.get("unid");
				if (documentUNIDArr == null || documentUNIDArr.length == 0) {
					docUNID = "";
				} else {
					docUNID = documentUNIDArr[0];
				}
			} else {
				docUNID = documentIdArr[0];
			}
		}
		// _logger.finest("Document UNID = " + docUNID);
		return docUNID;
	}

	/**
	 * Get the document object we're currently processing
	 * 
	 * @return lotus.domino.Document - the currentDocument we're processing
	 * @throws NotesException
	 */
	public Document getDocument() throws NotesException {
		String documentUNID = getDocUNID();
		if (documentUNID.equals("") || documentUNID == null) {
			isNewDoc = true;
			return null;
		} else {
			Database db = ExtLibUtil.getCurrentDatabase();
			Document doc = db.getDocumentByUNID(documentUNID);
			return doc;
		}
	}

	/**
	 * Clear all the values in cachedValues and changedValues
	 */
	private void clearAllValues() {
		cachedValues.clear();
		changedValues.clear();
	}

	/**
	 * Write a value to a notes item
	 * 
	 * @param doc - NotesDocument - The document we're currently editing/saving
	 * @param itemName - String - The name of the field we want to update
	 * @param itemValue - Object - The value to update the field with
	 */
	@SuppressWarnings("unchecked")
	private void writeItemValue(Document doc, String itemName, Object itemValue) {
		try {
			if (itemName.startsWith("My")) {
				MySQLUtil mySql = new MySQLUtil("mysql", "localhost", "3306", "lotusmysqldemo", "root", "Str1ckland2");
				ResultSet rs = mySql.executeQuery("SELECT * FROM data WHERE unid='" + docUNID + "'");
				if (itemValue == null) {
					itemValue = "";
				}
				if (rs == null || !rs.next()) {
					rs = mySql.executeQuery("INSERT INTO data (" + itemName + ",unid) VALUES ('" + itemValue.toString() + "','" + docUNID
							+ "')");
				} else {
					rs = mySql.executeQuery("UPDATE data SET " + itemName + "='" + itemValue + "' WHERE unid='" + docUNID + "'");
				}
				updateCachedValue(itemName, itemValue);
				mySql.closeAll();
			} else {
				doc.replaceItemValue(itemName, itemValue);
				updateCachedValue(itemName, itemValue);
			}
		} catch (NotesException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Update/add a value to the cachedValues map
	 * 
	 * @param keyName - String - the fieldname we want to update
	 * @param itemValue - Object - the value of the field
	 */
	private void updateCachedValue(String keyName, Object itemValue) {
		if (changedValues.containsKey(keyName)) {
			cachedValues.put(keyName, itemValue);
		}
	}

	public void redirectToSavedDoc(String unid) {
		try {
			ServletXSPContext thisContext = (ServletXSPContext) JSFUtil.getVariableValue("context");
			XSPUrl thisUrl = thisContext.getUrl();
			thisUrl.removeAllParameters();
			FacesContext.getCurrentInstance().getExternalContext().redirect(thisUrl + "?documentId=" + unid);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This isn't dependent on a document being open and is what populates our "view" data
	 * @param unid String - the UNID of the record we're looking up
	 * @param itemName String - The field name we're looking for
	 * @return String - The value from the mySQL db
	 */
	public String getSqlValue(String unid, String itemName) {
		String itemValue = null;
		try {
			MySQLUtil mySql = new MySQLUtil("mysql", "localhost", "3306", "lotusmysqldemo", "root", "Str1ckland2");
			ResultSet rs = mySql.executeQuery("SELECT " + itemName.toString() + " FROM data WHERE unid='" + unid + "'");
			if (rs.next()) {
				itemValue = rs.getString(itemName.toString());
			} else {
				itemValue = "NULL";
			}
			mySql.closeAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return itemValue;
	}

	/**
	 * Delete the record in Notes and mySQL
	 * @throws NotesException
	 */
	public void deleteRecord() throws NotesException {
		String unid = docUNID;
		Document thisDoc = getDocument();
		thisDoc.remove(true);
		MySQLUtil mySql = new MySQLUtil("mysql", "localhost", "3306", "lotusmysqldemo", "root", "Str1ckland2");
		ResultSet rs = mySql.executeQuery("DELETE FROM data WHERE unid='" + unid + "'");
		mySql.closeAll();
	}

	/************** Interface Methods *************/
	/**
	 * Save the Document we're currently working with in the browser
	 */
	@SuppressWarnings("null")
	public void save(String formName) {
		if (!changedValues.isEmpty()) {
			try {
				Document doc = getDocument();
				if (doc == null) {
					isNewDoc = true;
					Database db = ExtLibUtil.getCurrentDatabase();
					doc = db.createDocument();
					if (formName != null || !formName.equals("")) {
						doc.replaceItemValue("Form", formName);
					} else {
						doc.replaceItemValue("Form", "Post");
					}

					/**
					 * This is to give us a UNID to use in mySQL. Since all of our values are dependent on the UNID we've got to save a new
					 * document in order to get a UNID
					 */
					doc.save(true, false);
					docUNID = doc.getUniversalID();
				}
				for (String itemName : changedValues.keySet()) {
					Object itemValue = changedValues.get(itemName);
					writeItemValue(doc, itemName, itemValue);
				}
				doc.save(true, false);
				if (isNewDoc) {
					redirectToSavedDoc(docUNID);
				}
			} catch (NotesException e) {
				e.printStackTrace();
			}
			clearAllValues();
		} else {
			System.out.println("changedValues is empty");
		}
	}

	public void save() {
		save("DataDisplay");
	}

	/**
	 * Get the java Type for the field we're currently processing
	 */
	public Class<?> getType(Object fieldName) {
		return fieldName.getClass();
	}

	/**
	 * This is actually called by the JSF engine for each field to return that field's value. We then add that value to the cachedValues Map
	 * 
	 * @param itemName - The name of the field you want the value from
	 * @return Object - This should actually always be a string
	 */
	public Object getValue(Object itemName) {
		String idStr = itemName.toString();
		Object itemValue = null;
		if (changedValues.containsKey(idStr)) {
			return changedValues.get(idStr);
		} else if (cachedValues.containsKey(idStr)) {
			return cachedValues.get(idStr);
		} else {
			try {
				if (getDocument() == null || getDocUNID().equals("")) {
					return null;
				} else {
					if (itemName.toString().startsWith("My")) {
						String unid = docUNID;
						if (unid == null) {
							return itemValue;
						} else {
							try {
								MySQLUtil mySql = new MySQLUtil("mysql", "localhost", "3306", "lotusmysqldemo", "root", "Str1ckland2");
								ResultSet rs = mySql.executeQuery("SELECT " + itemName.toString() + " FROM data WHERE unid='" + unid + "'");
								if (rs.next()) {
									itemValue = rs.getString(itemName.toString());
								} else {
									itemValue = "";
								}
								mySql.closeAll();
							} catch (SQLException e) {
								e.printStackTrace();
							}
						}
						cachedValues.put(itemName.toString(), itemValue);
					} else {
						Document doc = getDocument();
						Item notesItem = doc.getFirstItem(itemName.toString());
						if (notesItem != null) {
							if (!notesItem.getValues().isEmpty()) {
								itemValue = notesItem.getValueString();
							} else {
								itemValue = "";
							}
							cachedValues.put(doc.getFirstItem(itemName.toString()).getName(), itemValue);
						}
					}
					return itemValue;
				}
			} catch (NotesException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				return null;
			}
		}
		return null;
	}

	/**
	 * Check if a field is read only
	 */
	public boolean isReadOnly(Object itemName) {
		return false;
	}

	/**
	 * This is actually called by the JSF engine to set the value for each field to what is currently on the page. We then add that value to
	 * the changedValues Map.
	 * 
	 * @param itemName - The name of the field you want the set the value of
	 * @param itemValue - The value to set the itemName field to
	 */
	public void setValue(Object itemName, Object itemValue) {
		Object oldValue = getValue(itemName);
		Boolean valueChanged = false;
		if ((oldValue != null && !itemValue.equals(oldValue)) || (itemValue != null && oldValue == null)) {
			valueChanged = true;
		}
		if (valueChanged) {
			if (itemValue == null || itemValue.equals("")) {
				changedValues.put(itemName.toString(), null);
			} else {
				changedValues.put(itemName.toString(), itemValue);
			}
		}
	}

	public class MySQLUtil {

		private Connection mConnection;
		private ResultSet mRS;
		private Statement mStatement;
		private String mUrl;

		public MySQLUtil(String sqlDriver, String hostName, String portNumber, String dataBase, String userName, String userPassword) {
			mUrl = "jdbc:" + sqlDriver + "://" + hostName;
			if (!portNumber.isEmpty() && portNumber != null) {
				mUrl = mUrl + ":" + portNumber;
			}
			mUrl = mUrl + "/" + dataBase + "?user=" + userName + "&password=" + userPassword;
			connectToDb();
		}

		/**
		 * @param connectUrl String - should be formatted like: jdbc:mysql://localhost:3306/databaseName?user=monty&password=greatsqldb
		 */
		public MySQLUtil(String connectUrl) {
			mUrl = connectUrl;
			connectToDb();
		}

		public void connectToDb() {
			try {
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				mConnection = DriverManager.getConnection(mUrl);
			} catch (IllegalAccessException e) {
				System.out.println("You are not authorized to access that DB");
				System.out.println("Message: " + e.getMessage());
			} catch (InstantiationException e) {
				System.out.println("Unable to connect to the DB");
				System.out.println("Message: " + e.getMessage());
			} catch (ClassNotFoundException e) {
				System.out.println("com.mysql.jdbc.Driver class not found");
				System.out.println("Message: " + e.getMessage());
				System.out.println("Exception: " + e.getException());
			} catch (SQLException e) {
				System.out.println("Message: " + e.getMessage());
				System.out.println("SQLState: " + e.getSQLState());
				System.out.println("VendorError: " + e.getErrorCode());
			}
		}

		public void connectToDb(String connectUrl) {
			mUrl = connectUrl;
			connectToDb();
		}

		public ResultSet executeQuery(String sqlQuery) {
			mRS = null;
			try {
				if (mConnection != null) {
					mStatement = mConnection.createStatement();
					if (mStatement.execute(sqlQuery)) {
						mRS = mStatement.getResultSet();
					}
				} else {
					System.out.println("Connection is null");
				}
			} catch (SQLException e) {
				System.out.println("Executing Query failed");
				System.out.println("Message: " + e.getMessage());
				System.out.println("SQLState: " + e.getSQLState());
				System.out.println("VendorError: " + e.getErrorCode());
			}
			return mRS;
		}

		public int getRowCount() {
			int rowCount = 0;
			try {
				if (mRS != null && mRS.last()) {
					mRS.last();
					rowCount = mRS.getRow();
					mRS.beforeFirst();
				}
			} catch (SQLException e) {
				System.out.println("Failed to get row count");
				System.out.println("Message: " + e.getMessage());
				System.out.println("SQLState: " + e.getSQLState());
				System.out.println("VendorError: " + e.getErrorCode());
			}
			return rowCount;
		}

		public void closeConnection() {
			try {
				if (mConnection != null) {
					mConnection.close();
				}
			} catch (SQLException e) {
				System.out.println("Closing the connection failed");
				System.out.println("Message: " + e.getMessage());
				System.out.println("SQLState: " + e.getSQLState());
				System.out.println("VendorError: " + e.getErrorCode());
			}
		}

		public void closeResultSet() {
			try {
				if (mRS != null) {
					mRS.close();
				}
			} catch (SQLException e) {
				System.out.println("Closing the ResultSet failed");
				System.out.println("Message: " + e.getMessage());
				System.out.println("SQLState: " + e.getSQLState());
				System.out.println("VendorError: " + e.getErrorCode());
			}
		}

		public void closeStatement() {
			try {
				if (mStatement != null) {
					mStatement.close();
				}
			} catch (SQLException e) {
				System.out.println("Closing the Statement failed");
				System.out.println("Message: " + e.getMessage());
				System.out.println("SQLState: " + e.getSQLState());
				System.out.println("VendorError: " + e.getErrorCode());
			}
		}

		public void closeAll() {
			try {
				if (mRS != null) {
					mRS.close();
				}
				if (mStatement != null) {
					mStatement.close();
				}
				if (mConnection != null) {
					mConnection.close();
				}
			} catch (SQLException e) {
				System.out.println("Closing resources failed");
				System.out.println("Message: " + e.getMessage());
				System.out.println("SQLState: " + e.getSQLState());
				System.out.println("VendorError: " + e.getErrorCode());
			}
		}
	}
}
