package com.keithstric.demo.interfaces;

public interface PageActionInterface {

	public Object execute(String args);
	
	public Object execute();
	
}
