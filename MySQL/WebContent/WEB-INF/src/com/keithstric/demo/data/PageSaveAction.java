package com.keithstric.demo.data;


public class PageSaveAction extends PageActionRedirect {

	// private static final Logger _logger = Logger.getLogger(PageSaveAction.class.getName());

	public Object execute(String formName) {
		// _logger.finer("PageSaveAction.execute(formName) starting for " + formName.toUpperCase());
		DataProvider dataProvider = DataProvider.getCurrentInstance();
		if (dataProvider != null) {
			// _logger.finer("start dataProvider.save(formName) with form " + formName);
			if (formName != null && !formName.equals("")) {
				dataProvider.save(formName);
			} else {
				dataProvider.save();
			}
		}
		return null;
	}

	@Override
	public Object execute() {
		// _logger.finer("PageSaveAction.execute() starting");
		execute("");
		return null;
	}

}
