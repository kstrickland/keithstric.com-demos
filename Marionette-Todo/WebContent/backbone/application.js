define([
	'marionette',
	'settings',
	'backbone/router.js',
	'backbone/controller.js',
	'backbone/modules/layout/layout_module.js'
], function(Marionette, Settings, Router, AppController, LayoutModule) {

	var App = new Backbone.Marionette.Application();

	/**
	 * The 'APP_CONTAINER' region is where the layout will be placed
	 */
	App.addRegions({
		'APP_CONTAINER': '#siteLayoutContainer'
	});

	/**
	 * A couple of helper methods to get the layout areas
	 */
	App.getLayoutView = function() {
		var containerRegion = this.getRegion('APP_CONTAINER');
		var layoutView = null;
		if ('currentView' in containerRegion && containerRegion.currentView) {
			layoutView = containerRegion.currentView;
		}
		return layoutView;
	};

	App.getContentArea = function() {
		return this.getLayoutView().getRegion('CONTENT_AREA');
	};

	/**
	 * Initializers are methods set to run when the App is starting. These will run
	 * in the order they are added and after the 'before:start' event but before the
	 * 'start' event.
	 *
	 * This initializer will setup anything that should be setup before Backbone.history.start
	 * runs. We have defined an ajaxPrefilter to define the host url (http://some.host.com) that should be used in
	 * all ajax requests.
	 */
	App.addInitializer(function(options) {
		// Determine the current protocol://host:port
        $.ajaxPrefilter(function(options, originalOptions, jqXhr) {
            if (options.url.indexOf('templates/_') === -1) {
                var prot = location.protocol;
                var host = location.hostname;
                var port = location.port;
                var url = prot + '//' + host;
                if (port) {
                    url += ':' + port;
                }
                url += options.url;
                options.url = url;
            }
        });

        /**
         * Add a listener for completed ajax requests to check if we get a
         * login page back from the server. If we do, redirect to the login
         * page and provide a redirectto argument
         */
        $(document).bind('ajaxComplete', function(evt, xhr, xhrOptions){
			var response = xhr.responseText ? xhr.responseText.toLowerCase() : xhr.responseText;
			var authUrl = Settings.AUTHENTICATION_URL.toLowerCase();
			if (response.indexOf('action="' + authUrl + '"') > -1) {
				location.href = authUrl + '&redirectto=' + location.href;
			}
		});

        /**
         * Add a getFirst and getLast method to Backbone collections. These will
         * return the first and last models respectively
         */
        Backbone.Collection.prototype.getLast = function() {
            return this.models[this.models.length -1];
        };
        Backbone.Collection.prototype.getFirst = function() {
            return this.models[0];
        };
        // Set a default URL
        Backbone.Model.url = Settings.DOC_REST_URL;
        Backbone.Marionette.View.prototype.currentBehaviors = [];
        Backbone.Marionette.Behavior.prototype.childViews = [];

        // This is the entry point into backbone
        this.Router = new Router();
		Backbone.history.start();
	});

	/**
	 * Startup Events - These events are fired when App.start() is called.
	 * 'before:start' runs before the application is started
	 * 'start' runs after the application is started
	 */
	App.on('before:start', function(options) {
		window.TodoApp = this;
		this.controller = new AppController();
		this.module('Layout', LayoutModule);
	});

	//App.start();
	return App;

});