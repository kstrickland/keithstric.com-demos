package com.keithstric.demo.data;

import java.util.logging.Logger;

import com.ibm.xsp.model.DataObject;
import com.keithstric.demo.interfaces.PageActionInterface;

public class PageActionBean implements DataObject {

	private static final Logger _logger = Logger.getLogger(PageActionBean.class.getName());

	public Class<?> getType(Object arg0) {
		_logger.finest("PageActionBean.getType running");
		return PageActionInterface.class;
	}

	public Object getValue(Object arg0) {
		_logger.finest("PageActionBean.getValue running");
		if (arg0.equals("save")) {
			return new PageSaveAction();
		}
		return null;
	}

	public boolean isReadOnly(Object arg0) {
		_logger.finest("PageActionBean.isReadOnly running");
		return true;
	}

	public void setValue(Object arg0, Object arg1) {
		_logger.finer("PageActionBean.setValue running");
	}

}
