define([
        'marionette',
        'settings'
], function(Marionette, Settings) {

	var AppController = Backbone.Marionette.Controller.extend({
		initialize: function(options) {
		
		},
        /**
         * Figure out the base URL (i.e. http://some.domain:8080)
         */
		getUrl: function() {
            var prot = location.protocol;
            var host = location.hostname;
            var port = location.port;
            var url = prot + "//" + host;
            if (port && port != 80) {
				url += ":" + port;
            }
            return url;
        },
        /**
		 * Update the header portion of the layout (the section right above the content area. If you want to set the title
		 * or sub title to blank, pass an empty string to title and/or subTitle
		 * @param {String} title - The title to assign, passing null will use the default value from the config
		 * @param {String} subTitle - The sub title to assign, passing null will use the default value from the config
		 */
		updateHeader: function(title, subTitle) {
            var headerTitle = $('h3.rpdHeaderTitleText');
            var headerSubTitle = $('h5.rpdHeaderSubTitleText');
            if (title || title === '') {
                headerTitle.html(title);
            }else{
                headerTitle.html(Settings.DEFAULT_HEADER);
            }
            if (subTitle || subTitle === '') {
                headerSubTitle.html(subTitle);
            }else{
                headerSubTitle.html(Settings.DEFAULT_SUBHEADER);
            }
        },
        /**
         * Update the window title
         * @param {String} title - Set the window title to the passed title, passing null will use the default value from the config
         */
        updateWindowTitle: function(title) {
            if (title) {
                document.title = title;
            }else{
                document.title = Settings.WINDOW_TITLE;
            }
        },
        /**
         * Update the text in the 'brand' link of the top Navbar
         * @param {String} title - Set the 'brand' text to the passed title, passing null will use the default value from the config
         */
        updateNavbarBrand: function(title) {
            var brandLink = $('a.navbar-brand');
            if (title) {
                brandLink.append(title);
            }else{
                brandLink.append(Settings.NAVBAR_BRAND_TEXT);
            }
        }
	});

	return AppController;

});
