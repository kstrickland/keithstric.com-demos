/**
 * This is the entry point into the application. This file defines the location of the base javascript files needed.
 */
requirejs.config({
	baseUrl: "./",
    urlArgs: "open&bustCache=" + (new Date()).getTime(), //This needs to be changed to a version number when in production
    shim: {
        jquery: {
            exports: '$'
        },
        underscore: {
            exports: '_'
        },
        bootstrap: {
            deps: ['jquery']
        },
        backbone: {
            deps: ['jquery', 'underscore'],
            exports: 'Backbone'
        },
        marionette: {
            deps: ['jquery', 'underscore', 'backbone'],
            exports: 'Marionette'
        }
    },
    paths: {
        jquery: 'lib/jquery',
        bootstrap: 'lib/bootstrap',
        underscore: 'lib/underscore',
        backbone: 'lib/backbone',
        marionette: 'lib/backbone.marionette',
        text: 'lib/text',
        settings: 'config/settings',
        application: 'backbone/application'
    },
    text: {
        env: 'xhr'
    }
});

require([
    'application'],
    function(App) {
        console.log("Ready to start the application");
        App.start();
    }
);