define([
	'marionette',
	'text! backbone/modules/layout/templates/LayoutTemplate.html'
], function(Marionette, LayoutHtml) {

	var LayoutView = Backbone.Marionette.LayoutView.extend({
		template: _.template(LayoutHtml),
		regions: {
			NAVBAR_LINKS: $("div.navbarLinks"),
			CONTENT_AREA: $("div#contentArea")
		},
		onRender: function() {

		}
	});

	return LayoutView;

});
