package com.keithstric.demo.data;

import java.util.logging.Logger;

import com.keithstric.demo.interfaces.PageActionInterface;

public abstract class PageActionRedirect implements PageActionInterface {

	private static final Logger _logger = Logger.getLogger(PageActionRedirect.class.getName());

	public Object execute() {
		_logger.finer("PageActionRedirect.execute starting");
		return execute();
	}

}
