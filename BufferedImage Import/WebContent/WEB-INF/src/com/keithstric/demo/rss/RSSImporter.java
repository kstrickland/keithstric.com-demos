package com.keithstric.demo.rss;

/*
 * This class does the actual import and creation of documents from the RSSFeed. 
 */

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.MIMEEntity;
import lotus.domino.MIMEHeader;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.Stream;

import com.ibm.domino.xsp.module.nsf.NotesContext;

public class RSSImporter {

	public RSSImporter() {
	}

	/**
	 * Take an RSSPost and import it as a Post document
	 * 
	 * @param post
	 *            RSSPost - The post we're importing
	 */
	public static void importRSSPost(RSSPost post) {
		try {
			System.out.println("Importing Post '" + post.getTitle() + "'");
			Database db = NotesContext.getCurrent().getCurrentDatabase();
			String postText = post.getContentText();
			for (RSSImage image : post.getImages()) {
				RSSImage img = importRSSImage(image);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Import the images found during the creation of the feed
	 * 
	 * @param image
	 *            - RSSImage
	 */
	public static RSSImage importRSSImage(RSSImage image) {
		System.out.println("Importing Image " + image.getFileName());
		Session sess = NotesContext.getCurrent().getCurrentSession();
		Database db = NotesContext.getCurrent().getCurrentDatabase();
		Document imageDoc;
		String fullUrl = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
		try {
			imageDoc = db.createDocument();
			imageDoc.replaceItemValue("Form", "image");
			imageDoc.save(true, false);
			String unid = imageDoc.getUniversalID();
			imageDoc.replaceItemValue("imageFileName", image.getFileName());
			String url = fullUrl + "/0/" + unid + "/$FILE/" + image.getFileName();
			imageDoc.replaceItemValue("imageNewUrl", url);
			imageDoc.replaceItemValue("imageExt", image.getExtension());
			imageDoc.replaceItemValue("imageOrigUrl", image.getOrigUrl());
			image.setNewPermalink(url);

			sess.setConvertMime(false);
			attachImage(imageDoc, image);
			sess.setConvertMime(true);
			imageDoc.closeMIMEEntities();

			imageDoc.save();
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return image;
	}

	private static void attachImage(Document imageDoc, RSSImage image) {
		try {
			MIMEEntity mime = imageDoc.createMIMEEntity("imageFile");
			MIMEHeader header = mime.createHeader("Content-Disposition");
			header.setHeaderVal("attachment; filename=\"" + image.getFileName() + "\"");
			header = mime.createHeader("Content-ID");
			header.setHeaderVal(image.getFileName());

			BufferedImage buffImg = image.getBaseImg();
			byte[] imgByteArray = getImgByteArray(buffImg, image.getExtension());
			InputStream is = new ByteArrayInputStream(imgByteArray);
			Stream stream = NotesContext.getCurrent().getCurrentSession().createStream();
			stream.setContents(is);
			mime.setContentFromBytes(stream, "image/" + image.getExtension(), mime.ENC_IDENTITY_BINARY);
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	private static byte[] getImgByteArray(BufferedImage buffImg, String imageExtension) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(buffImg, imageExtension, baos);
			byte[] imgByteArray = baos.toByteArray();
			return imgByteArray;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
