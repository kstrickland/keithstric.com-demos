define([
	'marionette',
	'backbone/modules/layout/views/layoutview.js'
], function(Marionette, LayoutView) {

	var LayoutModule = Backbone.Marionette.Module.extend({
		initialize: function(moduleName, App, options) {
			this.buildLayout(moduleName, App, options);
		},
		onStop: function() {

		},
		onStart: function() {
			if (!('currentView' in TodoApp.getRegion('APP_CONTAINER'))) {
				this.buildLayout('Layout', TodoApp);
			}
		},
		buildLayout: function(moduleName, App, options) {
			App.getRegion('APP_CONTAINER').show(new LayoutView());
			TodoApp.controller.updateHeader();
			TodoApp.controller.updateNavbarBrand();
			TodoApp.controller.updateWindowTitle();
		}
	});

	return LayoutModule;

});
