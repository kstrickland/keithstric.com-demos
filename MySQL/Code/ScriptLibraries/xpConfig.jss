/** 
Set base configuration for framework
Author: Steve Castledine, IBM
**/

function initBase(){ // setup defaults

// Reset Page Based Buttons
// Can be added anywhere using newButton("Link","Title")
// Can also be set in your XPage Custom Properties
viewScope.baseButtons=null;
baseButtons= new Array();

//Check for Buttons in Page Custom Properties
if (compositeData.buttonTitles!=null){
	for(var x=0;x<compositeData.buttonTitles.length;x++){
		newButton(compositeData.buttonTitles[x],compositeData.buttonTitles[x])
	}
}

//Application Settings
if(applicationScope.initConfig===null){
	/**
	*************************************************************
	Layout is setup in layout.properties (in Resources/Files)
	Can also be setup programmatically (do so in this function for max caching):
	
	Top Tab:				newTopTab("Link","Title")
	Main Tab:				newTab("Link","Title")
	Side Bar Menu Item:		newSideMenu("Link","Title")
	Footer Columns:			var col=new newFooterColumn("Column Title","StyleClass")	
	Footer Column Items:	col.addLink("Link","Title")
	Buttons:				newGlobalButton("Link","Title")
	*************************************************************
	**/
		
	// Create Layout from Properties File
	var createProperties=createFromProperties();
	
	//Check no errors in property file
	if(createProperties!=""){
		print ("Error In Layout Property File: "+database.getFilePath()+" : "+createProperties);
	}else{
		//Mark cache initialized		
		applicationScope.initConfig=true;
	}	
}	
	
//Session Settings
if(sessionScope.initConfig===null){
	
	//Basic setup
	sessionScope.path 				= facesContext.getExternalContext().getRequestContextPath();
	sessionScope.isSessionAuth		= @Contains(facesContext.getExternalContext().getRequest().getHeader("Cookie"), "DomAuthSessId") === 1 ? true : false; //dnt
	sessionScope.initConfig			= true;
	
	//Is Client? - Get session id
	if(@ClientType()=="Notes"){
		sessionScope.sessionID=facesContext.getExternalContext().getRequest().getSession().getId();
	}	
}	
}