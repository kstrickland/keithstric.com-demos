/** 
Set base configuration for framework - Utils
Author: Steve Castledine, IBM
**/

var baseTopTabs = new Array(); // Contains tabs (top of page) for framework
var baseTabs = new Array(); // Contains tabs for framework
var baseButtons = new Array(); // Contains buttons for framework
var baseGlobalButtons = new Array(); // Contains global buttons for framework
var baseSideMenus = new Array(); // Contains side menu items for framework
var baseFooterColumns = new Array(); // Contains footer columns for framework

function baseLink(link,title){ //Link Object
	this.link=link;
	this.title=title;	
}

function footerColumn(title,styleClass){ //Footer Column Object
	this.linkArray=new Array();
	
	this.title=title;
	this.styleClass=styleClass;
	
	this.prototype.addLink = function(link,title){
		this.linkArray[this.linkArray.length]=new baseLink(link,title);
	}		
}

function newTopTab(link,title){ //Add new top tab helper
	baseTopTabs[baseTopTabs.length]=new baseLink(link,title);	
	applicationScope.baseTopTabs=baseTopTabs;
}
function newTab(link,title){ //Add new tab helper
	baseTabs[baseTabs.length]=new baseLink(link,title);	
	applicationScope.baseTabs=baseTabs;
}
function newButton(link,title){ //Add new button helper
	baseButtons[baseButtons.length]=new baseLink(link,title);	
	viewScope.baseButtons=baseButtons;
}
function newGlobalButton(link,title){ //Add new global button helper
	baseGlobalButtons[baseGlobalButtons.length]=new baseLink(link,title);	
	applicationScope.baseGlobalButtons=baseGlobalButtons;
}
function newSideMenu(link,title){ //Add new side menu helper
	baseSideMenus[baseSideMenus.length]=new baseLink(link,title);	
	applicationScope.baseSideMenus=baseSideMenus;
}
function newFooterColumn(headerTitle,styleClass){ //Add new footer column helper	
	baseFooterColumns[baseFooterColumns.length]=new footerColumn(headerTitle,styleClass);
	applicationScope.baseFooterColumns=baseFooterColumns;
	
	return baseFooterColumns[baseFooterColumns.length-1];
}

function createFromProperties(){ //Creates Layout From Properties File
	var x=0;
	var errorMsg="";
	var headers=new Array();
	var titles=new Array();
	var links=new Array();
	
	try{
		errorMsg="Top Tabs";
		titles=layout.getString("topTabs.title").split(",");
		links=layout.getString("topTabs.link").split(",");
		for(var x=0;x<titles.length;x++){//Create Top Tabs		
			newTopTab(links[x],titles[x]);	
		}		
		
		errorMsg="Main Tabs";
		titles=layout.getString("mainTabs.title").split(",");
		links=layout.getString("mainTabs.link").split(",");
		for(var x=0;x<titles.length;x++){//Create Main Tabs	
			if(titles[x]!=""){newTab(links[x],titles[x])};	
		}
		
		errorMsg="Side Bar";
		titles=layout.getString("sideBarMenu.title").split(",");
		links=layout.getString("sideBarMenu.link").split(",");
		for(var x=0;x<titles.length;x++){//Create Side Bar		
			newSideMenu(links[x],titles[x]);	
		}
		
		var o=null;
		
		errorMsg="Footer Columns";
		headers=layout.getString("footerColumn.headers").split(",");
		
		for(var x=0;x<headers.length;x++){//Create Footer Columns
			o=newFooterColumn(headers[x]);	
			links=layout.getString("footerColumn"+(x+1)+".links").split(",");
			titles=layout.getString("footerColumn"+(x+1)+".titles").split(",");
			for(var y=0;y<links.length;y++){
				o.addLink(links[y],titles[y]);
			}			
		}		
		
		errorMsg="Global Buttons";
		titles=layout.getString("globalButtons.title").split(",");
		links=layout.getString("globalButtons.link").split(",");
		for(var x=0;x<titles.length;x++){//Create global Buttons	
			newGlobalButton(links[x],titles[x]);	
		}			
		
	}catch(e){
		return errorMsg+" : "+e;
	}
	return "";
}