package com.keithstric.demos.firststeps;

import java.util.Map;
import java.util.Vector;
import java.io.Serializable;

import javax.faces.context.FacesContext;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.Item;
import lotus.domino.NotesException;
import lotus.domino.View;

import com.ibm.domino.xsp.module.nsf.NotesContext;

public class BeanValues implements Serializable {
	
	public BeanValues() {
		System.out.println("WOOT!! Got us a BeanValues bean");
	}
	
	public void getBeanValues() {
		try {
			Database db = NotesContext.getCurrent().getCurrentDatabase();
			View view = db.getView("Default");
			Document doc = view.getDocumentByKey("BeanForm");
			String html = "<div>";
			if (doc != null) {
				Vector docItems = doc.getItems();
				for (Object docItemObj : docItems) {
					Item docItem = (Item) docItemObj;
					if (docItem.getName().indexOf("$") == -1) {
						html = html + docItem.getName() + " = " + docItem.getValueString() + "<br />";
					}
				}
			}else{
				html = "No Doc found";
			}
			html = html + "</div>";
			Map viewScope = (Map) getXVariableValue("viewScope");
			viewScope.put("BeanValues", html);
			
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}
	
	private Object getXVariableValue(String varName) {
		FacesContext context = FacesContext.getCurrentInstance();
		return context.getApplication().getVariableResolver().resolveVariable(context, varName);
	}
}
