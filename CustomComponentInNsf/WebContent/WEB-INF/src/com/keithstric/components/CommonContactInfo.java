package com.keithstric.components;

import javax.faces.FacesException;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.el.ValueBinding;

import com.ibm.xsp.component.FacesComponent;
import com.ibm.xsp.component.xp.XspInputText;
import com.ibm.xsp.component.xp.XspOutputLabel;
import com.ibm.xsp.component.xp.XspTable;
import com.ibm.xsp.component.xp.XspTableCell;
import com.ibm.xsp.component.xp.XspTableRow;
import com.ibm.xsp.page.FacesComponentBuilder;

public class CommonContactInfo extends UIComponentBase implements FacesComponent {

	private static final String RENDERER_TYPE = "com.keithstric.CommonContactInfo";
	private static final String COMPONENT_FAMILY = "com.keithstric";

	/**
	 * The component constructor. It is imperative that you run
	 * setRendererType here. The rendererType must match whatever
	 * you define in xsp-config and faces-config as the renderer-type
	 * for this component.
	 */
	public CommonContactInfo() {
		setRendererType(RENDERER_TYPE);
	}

	/**
	 * This method is necessary in order to define a family. The family is
	 * used to determine which renderer to use. Family must match whatever
	 * you define in xsp-config and faces-config as component-family.
	 */
	@Override
	public String getFamily() {
		return COMPONENT_FAMILY;
	}

	/**
	 * Here we build our table rows and add them to the table
	 * @param table - XspTable - The table that will hold our fields and labels
	 */
	@SuppressWarnings("unchecked")
	private void buildTableRows(XspTable table) {
		//We want 2 rows with 4 cells in each. This loop is for the rows
		for (int j = 0; j < 2; j++) {
			XspTableRow row = new XspTableRow();

			//This loop is for building the table cells
			for (int i = 0; i < 4; i++) {
				XspTableCell cell = new XspTableCell();
				Integer jInt = j + 1;
				Integer iInt = i + 1;
				String cellId = "cell" + jInt.toString() + "_" + iInt.toString();
				switch (i) {
					case 0:
						cell.setId(cellId);
						cell.setStyleClass("labelCol");
						//Create a new label
						XspOutputLabel label1;
						if (j == 0) {
							label1 = createLabel("First Name", "FirstName1");
						} else {
							label1 = createLabel("Email Address", "EmailAddress1");
						}
						//Add it to the table cell as a child
						cell.getChildren().add(label1);
						break;

					case 1:
						cell.setId(cellId);
						cell.setStyleClass("fieldCol");
						//Create a new field
						XspInputText input1;
						if (j == 0) {
							input1 = createField("FirstName");
						} else {
							input1 = createField("EmailAddress");
						}
						//Add it to the table cell as a child
						cell.getChildren().add(input1);
						break;

					case 2:
						cell.setId(cellId);
						cell.setStyleClass("labelCol");
						XspOutputLabel label2;
						if (j == 0) {
							label2 = createLabel("Last Name", "LastName1");
						} else {
							label2 = createLabel("Phone Number", "PhoneNumber1");
						}
						cell.getChildren().add(label2);
						break;

					case 3:
						cell.setId(cellId);
						cell.setStyleClass("fieldCol");
						XspInputText input2;
						if (j == 0) {
							input2 = createField("LastName");
						} else {
							input2 = createField("PhoneNumber");
						}
						cell.getChildren().add(input2);
						break;

					default:
						break;
				}
				//Add the cell to the row
				row.getChildren().add(cell);
			}
			//Add the row to the table
			table.getChildren().add(row);
		}
	}

	/**
	 * This method will create a label
	 * @param labelValue - String - The value of the label that will show on the form
	 * @param fieldFor - String - The ID of the field this label is for
	 * @return XspOutputLabel - A Label
	 */
	private XspOutputLabel createLabel(String labelValue, String fieldFor) {
		XspOutputLabel label = new XspOutputLabel();
		label.setValue(labelValue);
		label.setFor(fieldFor);
		label.setStyleClass("Label");
		return label;
	}

	/**
	 * This method will create an input text field
	 * @param fieldName - String - The field name on the actual Notes form
	 * @return - XspInputText - A Text Field
	 */
	private XspInputText createField(String fieldName) {
		//Create an inputText component
		XspInputText input = new XspInputText();
		input.setId(fieldName + "1");
		//Add a value binding to the field. This is responsible for adding a value to the document
		FacesContext context = FacesContext.getCurrentInstance();
		ValueBinding vb = context.getApplication().createValueBinding("#{document1." + fieldName + "}");
		input.setValueBinding("value", vb);
		return input;
	}

	/**
	 * This method will actually run after beforePageLoad and is the perfect opportunity
	 * to inject child components that will be built during the encodeChildren method
	 * of the renderer
	 */
	@SuppressWarnings("unchecked")
	public void initBeforeContents(FacesContext arg0) throws FacesException {
		XspTable table = new XspTable();
		buildTableRows(table);
		this.getChildren().add(table);
	}

	/**
	 * This method must be present because we're implementing FacesComponent
	 */
	public void buildContents(FacesContext arg0, FacesComponentBuilder arg1) throws FacesException {
		// Do Nothing		
	}

	/**
	 * This method must be present because we're implementing FacesComponent
	 */
	public void initAfterContents(FacesContext arg0) throws FacesException {
		// Do nothing
	}
}