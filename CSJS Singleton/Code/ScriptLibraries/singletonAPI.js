/*
 * This is an example of a singleton?
 */
var MyAPI = function() {
	var outside = "I'm running, jumping and playing outside";

	// This is object will be our public API
	var myapi = {
		someProperty: "Hello World!",
		someMethod: function() {
			alert(this.someProperty + " Go do something outside, " + outside);
		}
	}
	return myapi;
}();