function getSSJSValues() {
	var db:NotesDatabase = session.getCurrentDatabase();
	var view:NotesView = db.getView("Default");
	var doc:NotesDocument = view.getDocumentByKey("SSJSForm");
	if (doc != null) {
		var html = "<div>"
		for (var i=0;i < doc.getItems().size();i++) {
			var ssjsItem:NotesItem = doc.getItems().get(i);
			if (ssjsItem.getName().indexOf("$") == -1) {
				html += ssjsItem.getName() + " = " + ssjsItem.getValueString() + "<br />"
			}
		}
	}else{
		var html = "No Doc Found";
	}
	html += "</div>"
	viewScope.put("SSJSValues",html);
}