package com.keithstric.renderkit;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.Renderer;

public class CommonContactInfoRenderer extends Renderer {

	/**
	 * Here is the common encodeBegin method. This method is present in almost all renderers
	 * This is where we'll start opening tags and do anything that needs to be done before
	 * we start rendering children of the component.
	 */
	@Override
	public void encodeBegin(FacesContext context, UIComponent component) {
		try {
			super.encodeBegin(context, component);
			ResponseWriter writer = context.getResponseWriter();
			writer.startElement("fieldset", component);
			writer.writeAttribute("class", "ContactFieldset", null);
			writer.startElement("legend", component);
			writer.writeAttribute("class", "ContactLegend", null);
			writer.append("Contact Information");
			writer.endElement("legend");		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Here is the encodeChildren method. This method is present in almost all renderers.
	 * This is where you can wrap logic around the rendering of children or completely take
	 * over the rendering of children in order to add additional properties or whatever.
	 */
	@Override
	public void encodeChildren(FacesContext context, UIComponent component) {
		try {
			super.encodeChildren(context, component);		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Here is the encodeEnd method. This method is present in almost all renderers.
	 * This is where you close any tags you opened in encodeBegin and perform any
	 * logic that may be necessary after the children are encoded
	 */
	@Override
	public void encodeEnd(FacesContext context, UIComponent component) {
		try {
			super.encodeEnd(context, component);
			ResponseWriter writer = context.getResponseWriter();
			writer.endElement("fieldset");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
