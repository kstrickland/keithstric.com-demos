/**
 * These are basic and default settings for configuration of the application
 * DOC_REST_URL - The URL for Document REST service calls
 * VIEW_REST_URL - The URL for View REST service calls
 * REST_FETCH_LIMIT - The return limit for all lists
 * AUTHENTICATION_URL - The url for authentication
 * WINDOW_TITLE - The Default window title
 * DEFAULT_HEADER - The default header title
 * DEFAULT_SUBHEADER - The default sub header title
 * NAVBAR_BRAND_TEXT - The text of the brand in the navbar
 * INFINTE_SCROLL_TRIGGER - The number of pixels from the bottom of an area when an infinite scroll will be triggered
 */
define({
        DOC_REST_URL: "demo/rest.xsp/docRest",
        VIEW_REST_URL: "demo/rest.xsp/viewRest",
        REST_FETCH_LIMIT: 50,
        AUTHENTICATION_URL: "/names.nsf?login",
        WINDOW_TITLE: "Todo Demo",
        DEFAULT_HEADER: "Marionette Todo Demo",
        DEFAULT_SUBHEADER: "Welcome to the Marionette Todo Demo App",
        NAVBAR_BRAND_TEXT: "Todo Demo",
        INFINITE_SCROLL_TRIGGER: 300
});