package com.keithstric.demo.interfaces;

import com.ibm.xsp.model.DataObject;

public interface DataObjectEx extends DataObject {

	public void save(String formName);
	
	public void save();
}
