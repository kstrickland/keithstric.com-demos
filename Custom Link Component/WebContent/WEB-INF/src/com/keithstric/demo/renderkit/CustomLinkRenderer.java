package com.keithstric.demo.renderkit;

import java.io.IOException;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import com.ibm.xsp.component.xp.XspOutputLink;
import com.ibm.xsp.renderkit.html_extended.LinkRendererEx;
import com.keithstric.demo.components.CustomLink;

public class CustomLinkRenderer extends LinkRendererEx {

	@Override
	public void encodeBegin(FacesContext paramFacesContext, UIComponent paramUIComponent) throws IOException {
		super.encodeBegin(paramFacesContext, paramUIComponent);
		XspOutputLink localXspOutputLink = null;
		if (paramUIComponent instanceof CustomLink) {
			localXspOutputLink = (CustomLink) paramUIComponent;
		}
	}

	@Override
	public void encodeChildren(FacesContext paramFacesContext, UIComponent paramUIComponent) throws IOException {
		super.encodeChildren(paramFacesContext, paramUIComponent);
	}

	@Override
	public void encodeEnd(FacesContext paramFacesContext, UIComponent paramUIComponent) throws IOException {
		super.encodeEnd(paramFacesContext, paramUIComponent);
	}
}
