package com.demo.renderers;

import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

import com.ibm.commons.util.StringUtil;
import com.ibm.xsp.extlib.component.data.AbstractDataView;
import com.ibm.xsp.extlib.renderkit.html_extended.data.DataViewRenderer;
import com.ibm.xsp.extlib.util.ExtLibRenderUtil;

public class CustomDataViewRenderer extends DataViewRenderer {

	@Override
	protected void writeTableTagAttributes(FacesContext paramFacesContext, ResponseWriter paramResponseWriter,
			AbstractDataView paramAbstractDataView, ViewDefinition paramViewDefinition) throws IOException {
		paramResponseWriter.writeAttribute("id", paramAbstractDataView.getAjaxContainerClientId(paramFacesContext), null);
		String str1 = (String) getProperty(200);
		if (StringUtil.isNotEmpty(str1)) {
			str1 = str1 + " table table-striped table-bordered table-hover";
		} else {
			str1 = "table table-striped table-bordered table-hover";
		}
		paramResponseWriter.writeAttribute("class", str1, null);
		paramResponseWriter.writeAttribute("border", "0", null);
		paramResponseWriter.writeAttribute("cellspacing", "0", null);
		paramResponseWriter.writeAttribute("cellpadding", "0", null);
		String str2;
		if (!(paramViewDefinition.columnTitles)) {
			str2 = "presentation";
			paramResponseWriter.writeAttribute("role", str2, null);
		} else {
			str2 = null;
			if (!(ExtLibRenderUtil.isSummaryPresent(str2))) {
				str2 = "A collection of documents with a summary shown for each document";
			}
			if (ExtLibRenderUtil.isSummaryPresent(str2))
				paramResponseWriter.writeAttribute("summary", str2, null);
		}
	}

}
