package com.keithstric.demo.data;

import java.io.Serializable;
import java.util.TreeMap;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesException;

import com.keithstric.demo.utils.JSFUtils;
import com.keithstric.demo.utils.MIMEBean;

public class User implements Serializable {

	private String name;
	private TreeMap<String, Asset> assets;
	private String unid;

	public User() {
		loadDoc();
	}

	private void loadDoc() {
		try {
			if (getUnid() != null) {
				Database db = JSFUtils.getCurrentDatabase();
				String unid = getUnid();
				if (unid != null && !unid.isEmpty()) {
					Document doc;
					doc = db.getDocumentByUNID(unid);
					User desObj = (User) MIMEBean.restoreState(doc, "UserData");
					this.setName(desObj.getName());
					this.setAssets(desObj.getAssets());
					this.setUnid(desObj.getUnid());
				}
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	public void saveDoc() {
		try {
			Database db = JSFUtils.getCurrentDatabase();
			Document doc = null;
			String unid = getUnid();
			if (unid != null && !unid.isEmpty()) {
				doc = db.getDocumentByUNID(getUnid());
			} else {
				doc = db.createDocument();
				doc.replaceItemValue("form", "User");
				doc.save(true);
			}
			/*
			 * Doing this so that we can show data in a view
			 */
			doc.replaceItemValue("Name", getName());
			doc.replaceItemValue("unid", doc.getUniversalID());
			doc.save();

			/*
			 * Serialize this class. When displaying a user form to a user we'll
			 * deserialize this information.
			 */
			MIMEBean.saveState(this, doc, "UserData");
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TreeMap<String, Asset> getAssets() {
		if (assets != null) {
			return assets;
		} else {
			return new TreeMap<String, Asset>();
		}
	}

	public void setAssets(TreeMap<String, Asset> assets) {
		this.assets = assets;
	}

	public String getUnid() {
		if (unid != null) {
			return unid;
		}
		String docId = JSFUtils.getParamValue("documentId");
		if (docId != null) {
			return docId;
		} else {
			return null;
		}
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

}
