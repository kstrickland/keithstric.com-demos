define([
        'marionette'
], function(Marionette) {

	var RouterController = Backbone.Marionette.Controller.extend({
		home: function() {
			console.log("home route fired");
		}
	});

	return RouterController;

});
