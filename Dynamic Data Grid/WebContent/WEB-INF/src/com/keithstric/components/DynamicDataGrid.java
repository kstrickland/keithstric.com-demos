package com.keithstric.components;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.el.ValueBinding;

import lotus.domino.Database;
import lotus.domino.NotesException;
import lotus.domino.View;
import lotus.domino.ViewColumn;

import com.ibm.domino.xsp.module.nsf.NotesContext;
import com.ibm.xsp.component.UIScriptCollector;
import com.ibm.xsp.extlib.component.dojo.grid.UIDojoDataGrid;
import com.ibm.xsp.extlib.component.dojo.grid.UIDojoDataGridColumn;
import com.ibm.xsp.extlib.component.rest.DominoViewItemFileService;
import com.ibm.xsp.extlib.component.rest.UIRestService;

public class DynamicDataGrid extends UIComponentBase {

	private static String FAMILY = "com.keithstric";
	private static String COMPONENT_TYPE = "com.keithstric.DynamicDataGrid";
	
	private String viewName;
	private String excludeColumns;
	private String editableColumns;
	private boolean columnsMoveable = false;
	private boolean doubleClickOpensDoc = false;
	private String xpageName;
	
	public DynamicDataGrid() {}

	@SuppressWarnings("unchecked")
	@Override
	public void encodeBegin(FacesContext context) throws IOException {
		try {
			UIDojoDataGrid dataGrid = new UIDojoDataGrid();
			dataGrid.setStyle("height: 25em;");
			dataGrid.setStoreComponentId("ddgRestService");
			dataGrid.setColumnReordering(columnsMoveable);
			if (doubleClickOpensDoc) {
				dataGrid.setOnDblClick("var dataGridDij = dijit.byId(dojo.query(\"[id$='dynamicDataGrid']\")[0].id);" +
						"location.href = '" + xpageName + ".xsp?documentId=' + dataGridDij.selection.getSelected()[0].attributes['@unid'] + '&action=editDocument';"
				);
			}
			dataGrid.setId("dynamicDataGrid");
			String editableColumnsList = getEditableColumns().toLowerCase();
			String excludeColumnsList = getExcludeColumns().toLowerCase();
			Vector<ViewColumn> viewColumns = getViewColumns();
			if (!viewColumns.isEmpty()) {
				for (ViewColumn kid : viewColumns) {
					if (excludeColumnsList.indexOf(kid.getTitle().toLowerCase()) == -1) {
						UIDojoDataGridColumn column;
						boolean editable = false;
						if (editableColumnsList.indexOf(kid.getTitle().toLowerCase()) > -1) {
							editable = true;
						}
						column = createColumn(kid.getItemName(), kid.getTitle(), editable);
						dataGrid.getChildren().add(column);
					}
				}
				UIRestService restService = createRestService(viewName);
				this.getParent().getChildren().add(restService);
				this.getParent().getChildren().add(dataGrid);
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
		super.encodeBegin(context);
	}
	
	@Override
	public void encodeEnd(FacesContext context) throws IOException {
		super.encodeEnd(context);
		UIScriptCollector.find().addScript("XSP.addOnLoad(function(){" +
				"XSP.partialRefreshGet('" + this.getParent().getClientId(context) + "');" +
			"});"
		);
		//this.getParent().getRenderer(context).encodeBegin(context, this.getParent());
	}
	
	@SuppressWarnings("unused")
	private UIDojoDataGridColumn createColumn(String fieldName, String label, boolean isEditable) {
		UIDojoDataGridColumn ddgColumn = new UIDojoDataGridColumn();
		ddgColumn.setDraggable(columnsMoveable);
		ddgColumn.setEditable(isEditable);
		ddgColumn.setField(fieldName);
		ddgColumn.setLabel(label);
		ddgColumn.setWidth("auto");
		return ddgColumn;
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	private Vector<ViewColumn> getViewColumns() {
		try {
			Database db = NotesContext.getCurrent().getCurrentDatabase();
			View curView = db.getView(viewName);
			Vector<ViewColumn> columns = curView.getColumns();
			return columns;
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private UIRestService createRestService(String viewName) {
		DominoViewItemFileService restView = new DominoViewItemFileService();
		restView.setViewName(viewName);
		restView.setVar("entryRow");
		restView.setContentType("application/json");
		restView.setDefaultColumns(true);

		UIRestService restService = new UIRestService();
		restService.setId("ddgRestService");
		restService.setService(restView);
		return restService;
	}
	
	public String getViewName() {
		if (null != viewName) {
			return viewName;
		}
		ValueBinding vb = getValueBinding("viewName");
		if (vb != null) {
			return (String) vb.getValue(getFacesContext());
		} else {
			return null;
		}
	}

	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	public String getExcludeColumns() {
		if (null != excludeColumns) {
			return excludeColumns;
		}
		ValueBinding vb = getValueBinding("excludeColumns");
		if (vb != null) {
			return (String) vb.getValue(getFacesContext());
		} else {
			return "";
		}
	}

	public void setExcludeColumns(String excludeColumns) {
		this.excludeColumns = excludeColumns;
	}

	public String getEditableColumns() {
		if (null != editableColumns) {
			return editableColumns;
		}
		ValueBinding vb = getValueBinding("editableColumns");
		if (vb != null) {
			return (String) vb.getValue(getFacesContext());
		} else {
			return "";
		}
	}

	public void setEditableColumns(String editableColumns) {
		this.editableColumns = editableColumns;
	}

	public boolean isColumnsMoveable() {
		return columnsMoveable;
	}

	public void setColumnsMoveable(boolean columnsMovable) {
		this.columnsMoveable = columnsMovable;
	}

	@Override
	public String getFamily() {
		return FAMILY;
	}

	public boolean isDoubleClickOpensDoc() {
		return doubleClickOpensDoc;
	}

	public void setDoubleClickOpensDoc(boolean doubleClickOpensDoc) {
		this.doubleClickOpensDoc = doubleClickOpensDoc;
	}

	public String getXpageName() {
		if (null != xpageName) {
			return xpageName;
		}
		ValueBinding vb = getValueBinding("xpageName");
		if (vb != null) {
			return (String) vb.getValue(getFacesContext());
		} else {
			return null;
		}
	}

	public void setXpageName(String xpageName) {
		this.xpageName = xpageName;
	}
}