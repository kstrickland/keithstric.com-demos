package com.keithstric.demo.data;

import java.io.Serializable;

public class Asset implements Serializable {

	private String name;
	private Integer itemNumber;

	public Asset() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getItemNumber() {
		return itemNumber;
	}

	public void setItemNumber(Integer itemNumber) {
		this.itemNumber = itemNumber;
	}

}
